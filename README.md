# README #

### Key Takeaways ###
1. Search component does not yet have added pills as the user searchs. 
Implementing this is a little tricky; however, I have a few ideas as to how I can make 
this work (Semantic UI Dropdown, etc.).


### Instructions ###
1. `cd search-component`
2. `npm install`
3. `npm start`
4. Head over to http://localhost:3000/

