import React from 'react';
import styled from 'styled-components'

import SearchBar from './components/SearchBar'

const StyledDiv = styled.div`
  display: flex;
  justify-content: center;
`

function App() {
  return (
    <StyledDiv>
      <SearchBar />
    </StyledDiv>
  );
}

export default App;
