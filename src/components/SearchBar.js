import React from 'react';
import { map } from 'lodash'
import styled from 'styled-components'

import useDebounce from '../hooks/useDebounce'
import useOutsideClick from '../hooks/useOutsideClick'

const Autosuggest = styled.div`
  width: 280px;
  height: auto;
  max-height: 200px;
  border: 1px solid #ccc;
  border-top: none;
  overflow: auto;
  padding: 0;
`

const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`

const StyledInput = styled.input`
  direction: ltr;
  text-indent: 10px;
  width: 300px;
  height: 35px;
  border-radius: 50px;
  text-decoration: none;
  border: solid 1px #ccc;

  &:focus {
    outline: none;
  }
`

const StyledList = styled.ul`
  padding: 0px;
`

const StyledListItem = styled.li`
  padding: 10px;
  list-style-type: none;
  list-style-position: inside;
  margin: 0;
  font-family: Helvetica;

  &:hover {
    cursor: pointer;
    background-color: #AAD4FF;
  }
`

const SearchBar = () => {
  const [searchText, setSearchText] = React.useState('')
  const [selectedValue, setSelectedValue] = React.useState('')
  const [resultOptions, setResultOptions] = React.useState([])
  const debouncedSearch = useDebounce(searchText, 500)
  const wrapperRef = React.useRef(null);

  React.useEffect(() => {
    fetch(`http://www.omdbapi.com/?apikey=8822978d&s=${debouncedSearch}`)
    .then(response => response.json())
    .then(result => {
      const options = map(result.Search, (movie) => (
        {
          key: movie.imdbID,
          title: movie.Title
        }
      ))
      
      setResultOptions(options)
    })
  }, [debouncedSearch])

  const handleClickOutside = event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      // Empty the list of options.
      setResultOptions([])
    }
  };

  useOutsideClick(wrapperRef, handleClickOutside)

  const handleChange = (e) => {
    setSearchText(e.target.value)
  }

  const handleSelect = (value) => {
    setSelectedValue(value)
    setResultOptions([])
  }

  return (
    <StyledDiv ref={wrapperRef}>
      <StyledInput 
        onChange={handleChange} 
        onClick={() => setSelectedValue('')} 
        placeholder="Type a movie name ..." 
        value={selectedValue === '' ?  null : selectedValue} 
      />
      {resultOptions.length ? (
        <Autosuggest>
          <StyledList>
            {map(resultOptions, option => (
              <StyledListItem onClick={() => handleSelect(option.title)}>{option.title}</StyledListItem>
            ))}
          </StyledList>
        </Autosuggest>) 
        : null}
    </StyledDiv>
  )
}

export default SearchBar;
